package org.tooling.transactionverifier.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.tooling.transactionverifier.dto.FileRecords;
import org.tooling.transactionverifier.dto.Record;

class CsvRecordsReaderTest {

  @Test
  void csvFileIsSuccessfullyRead() throws Exception {
    final CsvRecordsReader recordsReader = new CsvRecordsReader();
    final File file = new File(this.getClass().getResource("/files/records.csv").getPath());
    final FileRecords records = recordsReader.readItems(file);

    assertNotNull(records);

    List<Record> transactions = records.getRecords();
    assertEquals(2, transactions.size());

    Record transaction = transactions.get(0);

    assertEquals("156108", transaction.getReferenceNumber());
    assertEquals("NL69ABNA0433647324", transaction.getAccountNumber());
    assertEquals("Flowers from Erik de Vries", transaction.getDescription());
    assertEquals(new BigDecimal("13.92"), transaction.getBalance());
    assertEquals(new BigDecimal("-7.25"), transaction.getMutation());
    assertEquals(new BigDecimal("6.67"), transaction.getEndBalance());

    transaction = transactions.get(1);

    assertEquals("112806", transaction.getReferenceNumber());
    assertEquals("NL93ABNA0585619023", transaction.getAccountNumber());
    assertEquals("Subscription from Rik Theuß", transaction.getDescription());
    assertEquals(new BigDecimal("77.29"), transaction.getBalance());
    assertEquals(new BigDecimal("-23.99"), transaction.getMutation());
    assertEquals(new BigDecimal("53.3"), transaction.getEndBalance());
  }

  @Test
  void csvFileWithWrongFormatThrowsException() {
    final CsvRecordsReader recordsReader = new CsvRecordsReader();
    final File file = new File(CsvRecordsReader.class.getResource("/files/records_wrong_format.csv").getPath());

    assertThrows(Exception.class, () -> recordsReader.readItems(file));
  }

}