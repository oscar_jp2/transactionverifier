package org.tooling.transactionverifier.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.tooling.transactionverifier.dto.ItemError;
import org.tooling.transactionverifier.dto.ReportError;

class TransactionsFailedReportWriterTest {

  private final TransactionsFailedReportWriter reportWriter = new TransactionsFailedReportWriter();

  @Test
  void reportSuccessfullyCreated(@TempDir File reportFolderTemp) throws Exception {
    final String referenceNumber = "NL69ABNA0433647324";
    final LocalDateTime transactionTime = LocalDateTime.now();
    final String filename = "failed_report";
    final String error = "Transaction error";

    final ItemError transaction = new ItemError(referenceNumber, transactionTime, filename, error);
    final ReportError reportError = new ReportError();
    reportError.setRecords(Collections.singletonList(transaction));

    reportWriter.write(reportFolderTemp, reportError);

    final Stream<Path> filesStream = Files.walk(Paths.get(reportFolderTemp.getPath()));
    List<File> files = filesStream
        .filter(Files::isRegularFile)
        .map(Path::toFile)
        .collect(Collectors.toList());

    assertTrue(CollectionUtils.isNotEmpty(files));
    assertEquals(1, files.size());
    assertTrue(files.get(0).getName().endsWith("_report.xml"));
  }

}