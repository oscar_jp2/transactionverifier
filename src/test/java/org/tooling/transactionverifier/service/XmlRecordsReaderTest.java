package org.tooling.transactionverifier.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.tooling.transactionverifier.dto.FileRecords;
import org.tooling.transactionverifier.dto.Record;

class XmlRecordsReaderTest {

  @Test
  void xmlFileIsSuccessfullyRead() throws Exception {
    final XmlRecordsReader recordsReader = new XmlRecordsReader();
    final File file = new File(this.getClass().getResource("/files/records.xml").getPath());
    final FileRecords records = recordsReader.readItems(file);

    assertNotNull(records);

    List<Record> transactions = records.getRecords();
    assertEquals(2, transactions.size());

    Record transaction = transactions.get(0);

    assertEquals("164702", transaction.getReferenceNumber());
    assertEquals("NL46ABNA0625805417", transaction.getAccountNumber());
    assertEquals("Flowers for Rik Dekker", transaction.getDescription());
    assertEquals(new BigDecimal("81.89"), transaction.getBalance());
    assertEquals(new BigDecimal("+5.99"), transaction.getMutation());
    assertEquals(new BigDecimal("87.88"), transaction.getEndBalance());

    transaction = transactions.get(1);

    assertEquals("189177", transaction.getReferenceNumber());
    assertEquals("NL27SNSB0917829871", transaction.getAccountNumber());
    assertEquals("Subscription for Erik Dekker", transaction.getDescription());
    assertEquals(new BigDecimal("5429"), transaction.getBalance());
    assertEquals(new BigDecimal("-939"), transaction.getMutation());
    assertEquals(new BigDecimal("6368"), transaction.getEndBalance());
  }

  @Test
  void xmlFileWithWrongFormatThrowsException() {
    final XmlRecordsReader recordsReader = new XmlRecordsReader();
    final File file = new File(this.getClass().getResource("/files/records_wrong_format.xml").getPath());

    assertThrows(RuntimeException.class, () -> recordsReader.readItems(file));
  }
}