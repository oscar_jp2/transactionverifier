package org.tooling.transactionverifier.adapter;

import static org.tooling.transactionverifier.Constants.DATE_TIME_FORMAT;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.simpleframework.xml.transform.Transform;


public class LocalDateTimeAdapter implements Transform<LocalDateTime> {

  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);

  @Override
  public LocalDateTime read(String value) {
    return LocalDateTime.parse(value, DATE_TIME_FORMATTER);
  }

  @Override
  public String write(LocalDateTime value) {
    return DATE_TIME_FORMATTER.format(value);
  }
}