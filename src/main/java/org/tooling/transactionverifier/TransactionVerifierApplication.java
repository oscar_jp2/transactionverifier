package org.tooling.transactionverifier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class TransactionVerifierApplication {

  public static void main(String[] args) {
    SpringApplication.run(TransactionVerifierApplication.class, args);
  }

}
