package org.tooling.transactionverifier;

public final class Constants {

  public static final String CSV_TYPE_EXT = "csv";
  public static final String XML_TYPE_EXT = "xml";
  public static final String REFERENCE_NUMBER_EXISTS_ERROR = "Reference Number already exists";
  public static final String END_BALANCE_NOT_CONSISTENT_ERROR = "End balance is not matching the expected balance";
  public static final String CLASSPATH_PROPERTY_PREFIX = "classpath:";
  public static final String BASE_FOLDER_REFERENCE = ".";
  public static final String CHARSET_ISO8859_1 = "ISO-8859-1";
  public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
