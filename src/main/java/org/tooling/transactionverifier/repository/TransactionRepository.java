package org.tooling.transactionverifier.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tooling.transactionverifier.dao.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, String> {

}
