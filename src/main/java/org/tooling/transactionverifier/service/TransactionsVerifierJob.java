package org.tooling.transactionverifier.service;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.tooling.transactionverifier.Constants;
import org.tooling.transactionverifier.dto.FileRecords;
import org.tooling.transactionverifier.dto.ItemError;
import org.tooling.transactionverifier.dto.ReportError;
import org.tooling.transactionverifier.error.FileNotValidException;
import org.tooling.transactionverifier.mapper.TransactionEntityMapper;
import org.tooling.transactionverifier.repository.TransactionRepository;

@Service
@Slf4j
public class TransactionsVerifierJob {

  private final TransactionRepository transactionRepository;

  private final CsvRecordsReader csvRecordsReader;

  private final XmlRecordsReader xmlRecordsReader;

  private final TransactionEntityMapper transactionEntityMapper;

  private final TransactionsFailedReportWriter transactionsFailedReportWriter;

  private final FilePathHandler filePathHandler;

  @Value("${transaction-file-names}")
  private List<String> transactionFileNames;

  @Value("${paths.create-missing-folders}")
  private boolean createMissingFolders;

  public TransactionsVerifierJob(TransactionRepository transactionRepository,
      CsvRecordsReader csvRecordsReader, XmlRecordsReader xmlRecordsReader,
      TransactionEntityMapper transactionEntityMapper, TransactionsFailedReportWriter transactionsFailedReportWriter,
      FilePathHandler filePathHandler) {
    this.transactionRepository = transactionRepository;
    this.csvRecordsReader = csvRecordsReader;
    this.xmlRecordsReader = xmlRecordsReader;
    this.transactionEntityMapper = transactionEntityMapper;
    this.transactionsFailedReportWriter = transactionsFailedReportWriter;
    this.filePathHandler = filePathHandler;
  }

  @Scheduled(cron = "${schedule.cron-scanning-expression}")
  public void scanFiles() {
    log.info("Starting new cycle");
    List<File> files = getTransactionFiles();

    if (CollectionUtils.isNotEmpty(files)) {
      files.forEach(this::processFile);
    } else {
      log.info("There are no transactions for parsing");
    }
  }

  private List<File> getTransactionFiles() {
    List<File> files = null;
    try {
      try (Stream<Path> paths = Files.list(Paths.get(filePathHandler.getScanPathFile().getPath()))) {
        files = paths
            .filter(Files::isRegularFile)
            .map(Path::toFile)
            .filter(file -> transactionFileNames.contains(file.getName()))
            .collect(Collectors.toList());
      }

    } catch (Exception e) {
      log.error("Path was not readable {}", filePathHandler.getScanPathFile(), e);
    }
    return files;
  }

  private void processFile(File file) {
    try {
      String fileExtension = getFileExtension(file);
      FileRecords records = getFileRecords(file, fileExtension);
      if (CollectionUtils.isNotEmpty(records.getRecords())) {
        generateErrorReport(file, records);
      }
      Files.move(Paths.get(file.getPath()), Paths.get(filePathHandler.getParsedPathFile().getPath()
          .concat(File.separator).concat(file.getName())));

    } catch (Exception e) {
      log.error("Extension not readable or not expected for file {}", file.getName(), e);
    }
  }

  private String getFileExtension(File file) {
    String filename = file.getName();
    int lastIndexOf = filename.lastIndexOf(".");
    if (lastIndexOf == -1) {
      return StringUtils.EMPTY;
    }
    String fileExtension = filename.substring(lastIndexOf + 1);
    if (StringUtils.isEmpty(fileExtension) || (!Constants.CSV_TYPE_EXT.equalsIgnoreCase(fileExtension)
        && !Constants.XML_TYPE_EXT
        .equalsIgnoreCase(fileExtension))) {
      throw new FileNotValidException(file.getName());
    }
    return fileExtension;
  }

  private void generateErrorReport(File file, FileRecords records) {
    ReportError reportError = new ReportError();
    records.getRecords().forEach(record ->
    {
      BigDecimal expectedAmount = record.getBalance().add(record.getMutation()).stripTrailingZeros();

      if (transactionRepository.existsById(record.getReferenceNumber())) {
        reportError.getRecords().add(new ItemError(record.getReferenceNumber(), LocalDateTime.now(), file.getName(),
            Constants.REFERENCE_NUMBER_EXISTS_ERROR));
      } else if (!expectedAmount.equals(record.getEndBalance())) {
        reportError.getRecords().add(new ItemError(record.getReferenceNumber(), LocalDateTime.now(), file.getName(),
            Constants.END_BALANCE_NOT_CONSISTENT_ERROR));
      } else {
        transactionRepository.save(transactionEntityMapper.apply(record));
      }

    });

    if (!reportError.getRecords().isEmpty()) {
      transactionsFailedReportWriter.write(filePathHandler.getReportPathFile(), reportError);
    }
  }

  private FileRecords getFileRecords(File file, String fileExtension) throws Exception {
    return Constants.CSV_TYPE_EXT.equalsIgnoreCase(fileExtension) ? csvRecordsReader.readItems(file) : xmlRecordsReader
        .readItems(file);
  }

}
