package org.tooling.transactionverifier.service;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.tooling.transactionverifier.Constants;
import org.tooling.transactionverifier.error.PathsNotConfiguredException;
import org.tooling.transactionverifier.error.PathsNotReadableException;

@Component
@Getter
public class FilePathHandler {

  private File scanPathFile;

  private File parsedPathFile;

  private File reportPathFile;

  public FilePathHandler(
      @Value("${paths.scanning-files-folder}") String scanningPath,
      @Value("${paths.parsed-files-folder}") String parsedPath,
      @Value("${paths.report-files-folder}") String reportPath,
      @Value("${paths.create-missing-folders}") boolean createMissingFolders) {

    if (scanningPath == null) {
      throw new PathsNotConfiguredException("paths.scanning-files-folder");
    } else {
      scanPathFile = convertToFile(scanningPath);
    }

    if (parsedPath == null) {
      throw new PathsNotConfiguredException("paths.parsed-files-folder");
    } else {
      parsedPathFile = convertToFile(parsedPath);
    }

    if (reportPath == null) {
      throw new PathsNotConfiguredException("paths.report-files-folder");
    } else {
      reportPathFile = convertToFile(reportPath);
    }

    if (createMissingFolders) {
      createFolderIfNotExists(Arrays.asList(scanPathFile, parsedPathFile, reportPathFile));
    }

  }

  private File convertToFile(String path) {
    if (path.trim().contains(Constants.CLASSPATH_PROPERTY_PREFIX)) {
      path = path.replace(Constants.CLASSPATH_PROPERTY_PREFIX, "");
      ClassLoader cl = ClassUtils.getDefaultClassLoader();
      String baseResourcePath = getBaseResourcePath(cl);
      baseResourcePath = baseResourcePath.concat(path);
      return new File(baseResourcePath);
    }
    return new File(path);
  }

  private String getBaseResourcePath(ClassLoader cl) {
    if (cl != null) {
      URL urlResource = cl.getResource(Constants.BASE_FOLDER_REFERENCE);
      if (urlResource != null) {
        return urlResource.getPath();
      }
    }
    return ClassLoader.getSystemResource(Constants.BASE_FOLDER_REFERENCE).getPath();
  }

  private void createFolderIfNotExists(List<File> folders) {
    folders.forEach(folder -> {
      try {
        Files.createDirectories(Paths.get(folder.getPath()));
      } catch (IOException e) {
        throw new PathsNotReadableException(folder.getPath());
      }
    });
  }

}
