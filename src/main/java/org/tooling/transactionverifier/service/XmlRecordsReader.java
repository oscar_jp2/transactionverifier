package org.tooling.transactionverifier.service;


import java.io.File;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.stereotype.Service;
import org.tooling.transactionverifier.dto.FileRecords;

@Service
public class XmlRecordsReader {

  FileRecords readItems(final File file) throws Exception {
    Serializer serializer = new Persister();
    return serializer.read(FileRecords.class, file);
  }

}
