package org.tooling.transactionverifier.service;


import static org.tooling.transactionverifier.Constants.CHARSET_ISO8859_1;

import com.opencsv.bean.CsvToBeanBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.List;
import org.springframework.stereotype.Service;
import org.tooling.transactionverifier.dto.FileRecords;
import org.tooling.transactionverifier.dto.Record;

@Service
public class CsvRecordsReader {

  FileRecords readItems(final File file) throws Exception {
    final Reader reader = new BufferedReader(
        new InputStreamReader(new FileInputStream(file), Charset.forName(CHARSET_ISO8859_1)));

    final List<Record> records = new CsvToBeanBuilder<Record>(reader)
        .withType(Record.class)
        .withIgnoreLeadingWhiteSpace(true)
        .build().parse();

    return new FileRecords(records);
  }

}
