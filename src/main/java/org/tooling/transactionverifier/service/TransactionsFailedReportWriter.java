package org.tooling.transactionverifier.service;


import java.io.File;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.transform.RegistryMatcher;
import org.springframework.stereotype.Service;
import org.tooling.transactionverifier.adapter.LocalDateTimeAdapter;
import org.tooling.transactionverifier.dto.ReportError;

@Service
@Slf4j
public class TransactionsFailedReportWriter {

  void write(final File reportPath, final ReportError reportError) {

    File file = new File(reportPath, LocalDateTime.now().getNano() + "_report.xml");
    RegistryMatcher registryMatcher = new RegistryMatcher();
    registryMatcher .bind(LocalDateTime.class, new LocalDateTimeAdapter());
    Serializer serializer = new Persister(registryMatcher);
    try {
      serializer.write(reportError, file);
    } catch (Exception e) {
      log.error("Report was not generated, please check the verifier path exists and has permissions: {}", reportPath,
          e);
    }
  }

}
