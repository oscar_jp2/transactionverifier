package org.tooling.transactionverifier.error;


public class PathsNotReadableException extends RuntimeException {

  public PathsNotReadableException(String path) {
    super(
        "Path not correcly defined or without writing rights, please check permissions or update application.yml file. Failed path: "
            + path);
  }

}
