package org.tooling.transactionverifier.error;


public class PathsNotConfiguredException extends RuntimeException {

  public PathsNotConfiguredException(String property) {
    super(
        "Path not correcly defined or without writing rights, please check permissions or update application.yml file. Failed property: "
            + property);
  }

}
