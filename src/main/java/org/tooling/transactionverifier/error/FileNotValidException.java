package org.tooling.transactionverifier.error;


public class FileNotValidException extends RuntimeException {

  public FileNotValidException(final String filename) {
    super("File not correct. Please check permissions and extension: " + filename);
  }
}
