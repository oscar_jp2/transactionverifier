package org.tooling.transactionverifier.mapper;

import java.time.LocalDateTime;
import java.util.function.Function;
import org.springframework.stereotype.Component;
import org.tooling.transactionverifier.dao.Transaction;
import org.tooling.transactionverifier.dto.Record;

@Component
public class TransactionEntityMapper implements Function<Record, Transaction> {

  @Override
  public Transaction apply(final Record record) {
    return new Transaction(
        record.getReferenceNumber(),
        record.getAccountNumber(),
        record.getDescription(),
        record.getBalance(),
        record.getMutation(),
        record.getEndBalance(),
        LocalDateTime.now());
  }

}
