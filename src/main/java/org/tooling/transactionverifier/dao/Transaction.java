package org.tooling.transactionverifier.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Transaction {

  @Id
  private String referenceNumber;
  private String accountNumber;
  private String description;
  private BigDecimal balance;
  private BigDecimal mutation;
  private BigDecimal endBalance;
  private LocalDateTime parsedTime;
}
