package org.tooling.transactionverifier.dto;


import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.simpleframework.xml.Element;

@Getter
@AllArgsConstructor
public class ItemError {

  @Element
  private String referenceNumber;

  @Element
  private LocalDateTime dateTimeJob;

  @Element
  private String filename;

  @Element
  private String error;

}
