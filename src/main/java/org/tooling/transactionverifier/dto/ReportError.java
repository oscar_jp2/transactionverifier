package org.tooling.transactionverifier.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Root(name = "errors")
public class ReportError {

  @ElementList(inline = true)
  private List<ItemError> records = new ArrayList<>();
}
