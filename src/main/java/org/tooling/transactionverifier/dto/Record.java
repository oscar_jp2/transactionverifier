package org.tooling.transactionverifier.dto;

import com.opencsv.bean.CsvBindByName;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Record {

  @Attribute(name = "reference")
  @CsvBindByName(column = "Reference")
  private String referenceNumber;

  @Element
  @CsvBindByName(column = "Account Number")
  private String accountNumber;

  @Element
  @CsvBindByName(column = "Description")
  private String description;

  @Element(name = "startBalance")
  @CsvBindByName(column = "Start Balance")
  private BigDecimal balance;

  @Element
  @CsvBindByName(column = "Mutation")
  private BigDecimal mutation;

  @Element
  @CsvBindByName(column = "End Balance")
  private BigDecimal endBalance;
}
