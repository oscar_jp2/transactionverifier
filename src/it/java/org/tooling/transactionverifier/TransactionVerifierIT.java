package org.tooling.transactionverifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.aspectj.util.FileUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.tooling.transactionverifier.repository.TransactionRepository;
import org.tooling.transactionverifier.service.TransactionsVerifierJob;

@SpringBootTest
class TransactionVerifierIT {

  @Value("${paths.scanning-files-folder}")
  private File scanPathFile;

  @Value("${paths.parsed-files-folder}")
  private File parsedPathFile;

  @Value("${paths.report-files-folder}")
  private File reportPathFile;

  @Value("${transaction-file-names}")
  private List<String> transactionFileNames;

  @Autowired
  private TransactionsVerifierJob transactionsVerifierJob;

  @Autowired
  private TransactionRepository transactionRepository;

  @BeforeEach
  void setUp() {
    FileUtil.deleteContents(scanPathFile);
    FileUtil.deleteContents(reportPathFile);
    FileUtil.deleteContents(parsedPathFile);
    transactionRepository.deleteAll();
  }

  @Test
  void filesWithoutErrorsDoNotGenerateReport() throws Exception {
    FileUtil.copyFile(new File(this.getClass().getResource("/transactions/records_all_success.csv").getPath()),
        scanPathFile);
    FileUtil.copyFile(new File(this.getClass().getResource("/transactions/records_all_success.xml").getPath()),
        scanPathFile);

    transactionsVerifierJob.scanFiles();

    assertTrue(getAllFiles(reportPathFile.getPath()).isEmpty());
    assertTrue(getAllFiles(scanPathFile.getPath()).isEmpty());

    List<File> parsedFiles = getAllFiles(parsedPathFile.getPath());
    assertTrue(CollectionUtils.isNotEmpty(parsedFiles));
    assertEquals(2, parsedFiles.size());
  }

  @Test
  void filesWithErrorsGeneratesReport() throws Exception {
    FileUtil.copyFile(new File(this.getClass().getResource("/transactions/records_with_error.csv").getPath()),
        scanPathFile);
    FileUtil.copyFile(new File(this.getClass().getResource("/transactions/records_with_error.xml").getPath()),
        scanPathFile);

    transactionsVerifierJob.scanFiles();

    assertFalse(getAllFiles(reportPathFile.getPath()).isEmpty());
    assertTrue(getAllFiles(scanPathFile.getPath()).isEmpty());

    List<File> parsedFiles = getAllFiles(parsedPathFile.getPath());
    assertTrue(CollectionUtils.isNotEmpty(parsedFiles));
    assertEquals(2, parsedFiles.size());
  }

  private List<File> getAllFiles(String folder) throws Exception {
    return Files.list(Paths.get(folder))
        .filter(Files::isRegularFile)
        .map(Path::toFile)
        .collect(Collectors.toList());
  }

}
