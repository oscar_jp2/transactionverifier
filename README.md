# TRANSACTION VERIFIER
This project reads and verifies XML and CSV files. Requeriments for a valid transaction are:

* Transaction reference should be unique
* End balance needs to be validated and should match the sum of Start Balance and Mutable fields.

Once the transaction file is read:
 - reports from failed transactions are saved in the report directory
 - files are moved to the parsed folder. This operation will repeat the scanning process according schedule.cron-scanning-expression`

### Getting Started Requeriments
Please, be sure mandatory properties are filled with expected values according your Operating System.

### Start Application
* From the root folder of the project in a Console or Terminal type:
  - `mvn clean install`
  - `java -Dspring.profiles.active=local -cp target/classes -jar target/transaction-verifier-0.0.1-SNAPSHOT.jar`  
  
## Properties from application.yml

- paths
   (Unix paths should be like: /a/b/c while Windows paths like: c:/a/b/c)
  - scanning-files-folder (Mandatory) Contains the path to an existing folder where transaction files will be read.
  - parsed-files-folder (Mandatory) Contains the path to an existing folder where transaction parsed files will be moved.
  - report-files-folder (Mandatory) Contains the path to an existing folder where a report with transaction failed files will be published.
  - create-missing-folders (Optional) boolean value. If true, application will try to create the previous folders 

- schedule.cron-scanning-expression (Mandatory) A valid cron expression for the job to run.
  For instance, every five minutes: '* */5 * * * ?', every five seconds: '*/5 * * * * ?', never: '-'
  - [Cron expressions](https://web.archive.org/web/20150317024908/http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/tutorial-lesson-06)
- transaction-file-names (Mandatory) Expected transaction file names

- spring:
  - datasource
    - url (Mandatory) Database URL
    - username (Mandatory) Database username
    - password (Mandatory) Database password
    - driverClassName (Mandatory) Database driver java class
  - jpa:
    - spring.jpa.database-platform (Optional) JPA Dialect class used by the DB Engine
    - hibernate.ddl-auto Indicates (Optional) if databases are created, updated or no changes on start

